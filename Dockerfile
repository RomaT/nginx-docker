FROM nginx:1.19

ARG USER
ARG GROUP
ARG UID
ARG GID
ARG HOME=/home/$USER

RUN groupadd --gid $GID $GROUP \
  && useradd --uid $UID --gid $GID --shell /bin/bash --create-home --home $HOME $USER

COPY host.conf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf

RUN touch /var/run/nginx.pid \
 && mkdir /socket \
 && chown -Rf $USER:$GROUP \
    /var/run/nginx.pid \
    /var/cache/nginx \
    /var/log/nginx \
    /socket

USER $USER:$GROUP
